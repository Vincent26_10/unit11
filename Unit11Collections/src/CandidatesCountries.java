import java.io.*;
import java.util.*;
class  Pair implements Comparable<Pair>{
	private int freq;
	private String country;
	
	public Pair(String country,int freq) {
		this.country=country;
		this.freq=freq;
	}

	@Override
	public int compareTo(Pair arg0) {
		
		return freq - arg0.freq;
	}
	
	public String toString() {
		return country +","+freq;
	}
	
	
}

public class CandidatesCountries {

	public static final String FILE_NAME = "candidatos.csv";

	public static void main(String[] args) throws IOException {
		OrderMap(takeDataFromFiles());
	}

	public static Map<String, Integer> takeDataFromFiles() throws IOException {
		Map<String, Integer> m = new HashMap<String, Integer>();
		BufferedReader in = null;
		String line;

		try {
			in = new BufferedReader(new FileReader(FILE_NAME));
			while ((line = in.readLine()) != null) {
				String[] items = line.split(",");
				String country = items[6];
				Integer freq = m.get(country);
				m.put(country, freq == null ? 1 : freq +1);
			}
		} finally {
			if(in != null) {
				in.close();
			}
		}
		return m;

	}
	
	public static void OrderMap(Map<String, Integer> m)  { 
		List<Pair> tempList = new ArrayList<Pair>();
		Set<String> keys = m.keySet();
		for(String key:keys) {
			int freq = m.get(key);
			Pair p = new Pair(key,freq);
			tempList.add(p);
			
		}
		Collections.sort(tempList);
		
		for(Pair p: tempList) {
			System.out.println(p);
		}
	}

}
