
public class Player  implements Comparable<Player>{
	private String name;
	private int Strength;
	
	public Player(String name, int Strength) {
		this.name=name;
		this.Strength=Strength;
	}
	
	@Override
	public String toString() {
		return name +" ("+Strength+")";
	}

	@Override
	public int compareTo(Player arg0) {
		return Strength - arg0.Strength;
		
	}

}
