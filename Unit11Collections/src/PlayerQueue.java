import java.util.*;

public class PlayerQueue {
	
	public static void main(String[] args) {
		
		
		Queue<Player> queue = new PriorityQueue<Player>();
		
		queue.add(new Player("Vincent",20000000));
		queue.add(new Player("Escanor",2000000));
		queue.add(new Player("Arceus",200000));		
		queue.add(new Player("Kirby",20000));
		
		while (!queue.isEmpty()) {
			System.out.println(queue.remove());
		}
		
	}
}
