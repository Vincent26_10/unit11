import java.io.*;
import java.util.*;

public class LastNameFreq {

	private static final String FILE_NAME = "LastnameFrequencies.csv";

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		try {
			Map<String, String> map = getMapFromFile();
			String lastName = "xxx";
			while (!lastName.equals("")) {
				System.out.println("Enter lastname: ");
				lastName = input.nextLine();
				if (!lastName.equals("")) {
					System.out.println("Frequency = " + map.get(lastName.toUpperCase()));
				}
			}
		} catch (IOException e) {
			System.err.println("Error reading file " + FILE_NAME);
		}

		input.close();

	}

	private static Map<String, String> getMapFromFile() throws IOException {
		Map<String, String> mapTemp = new HashMap<String, String>();
		BufferedReader in = null;

		try {
			in = new BufferedReader(new FileReader(FILE_NAME));

			String line;
			while ((line = in.readLine()) != null) {
				String[] items = line.split(",");
				mapTemp.put(items[0], items[1]);

			}
		} finally {
			if (in != null) {
				in.close();
			}
		}
		return mapTemp;
	}

}
