import java.util.*;

public class Freq {

	public static void main(String[] args) {
		Map<String, Integer > m = new HashMap<String,Integer>();
		for(String s:args) {
			Integer freq = m.get(s);
			
			m.put(s, freq == null ? 1 : freq + 1);
			
		}
		System.out.println(m);
	}

}
