import java.io.*;
import java.util.*;


public class CollectionsExercises {
	
	private static String nameOfFile;
	private static File f;
	private static File creator;

	public static void main(String[] args) {
		String[] s = {"hola","g","h"};
		sortArray(s);
		//shufflesArray(s);
		for(String str: s) {
			System.out.println(str);
		}
	}
	
	public static <E extends Comparable<E>>void sortArray(E[] s) {
		List<E> list = Arrays.asList(s);
		Collections.sort(list);
		list.toArray(s);
	}
	public static <E>void shufflesArray(E[] s) {
		List<E> list = Arrays.asList(s);
		Collections.shuffle(list);
		list.toArray(s);
	}
	
	private static void fileCreator(String nameOfFileCreator) throws IOException {
		nameOfFile = nameOfFileCreator;
		f = new File("tmp");
		creator = new File(nameOfFile);
		if (!creator.exists()) {
			creator.createNewFile();
		}
	}
	
	public static void sortFile(String name) throws IOException{
		fileCreator(name);
		BufferedReader in = null;
		PrintWriter out = null;
		
		try {
			in = new BufferedReader(new FileReader(name));
			out = new PrintWriter(new FileWriter(f));
			
			List<String> list = new ArrayList<String>();
			String line = null;
			
			while((line=in.readLine())!= null) {
				list.add(line);
			}
			Collections.sort(list);
			for(String s: list) {
				out.println(s);
			}
			f.renameTo(new File(name));
		}finally {
			if(in!=null) {
				in.close();
			}
			if(out!=null) {
				out.close();
			}
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
