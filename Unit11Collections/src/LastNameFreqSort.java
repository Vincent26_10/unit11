import java.util.*;
import java.io.*;

public class LastNameFreqSort {
	private static final String FILE_NAME = "LastnameFrequencies.csv";


	public static void main(String[] args) throws IOException {
		
		PrintWriter out = null;
		
		try {
		
		out = new PrintWriter(new FileWriter("LastNameSorted.csv"));
			
		Map<String, String> map = getMapFromFile();
		Set<String> keys = map.keySet();
		for(String key:keys) {
			String freq = map.get(key);
			out.println(key + "," + freq);
			
		}
		
		/*Set<Entry<String,String>> entries = map.entrySet();
		for(Entry<String,String> entry: entries) {
			String key = entry.getKey();
			String value = entry.getValue();
		}*/
		}finally {
			if(out != null) {
				out.close();
			}
		}
	}
	
	
	private static Map<String, String> getMapFromFile() throws IOException {
		Map<String, String> mapTemp = new TreeMap<String, String>();
		BufferedReader in = null;

		try {
			in = new BufferedReader(new FileReader(FILE_NAME));

			String line;
			while ((line = in.readLine()) != null) {
				String[] items = line.split(",");
				mapTemp.put(items[0], items[1]);

			}
		} finally {
			if (in != null) {
				in.close();
			}
		}
		return mapTemp;
	}
	

}
