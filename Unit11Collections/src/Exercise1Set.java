import java.util.*;

public class Exercise1Set {

	public static void main(String[] args) {
		/*
		 * Set<Integer> set = new HashSet<Integer>(); set.add(2); set.add(3);
		 * set.add(2); set.add(90); set.add(5); set.add(34); set.add(78); set.add(10);
		 * printSet(set); System.out.println("------"); printSet(increment(set));
		 * System.out.println("------");
		 */
		String[] s = { "hola", "victor", "q", "tal", "hola", "victor", "q", "tal", "e" };
		// remove1(s);
		printDup(s);

	}

	public static void printSet(Set<?> s) {
		for (Object i : s) {
			System.out.println(i);
		}
	}

	public static Set<Integer> increment(Set<Integer> s) {
		Set<Integer> set2 = new HashSet<Integer>();
		for (Integer i : s) {
			set2.add(i + 1);
		}
		return set2;
	}

	public static void remove1(String[] s) {
		Set<String> set2 = new HashSet<String>();
		for (String i : s) {
			set2.add(i);
		}
		printSet(set2);

	}

	public static void remove2(String[] s) {
		List<String> list = Arrays.asList(s);
		Set<String> set = new HashSet<String>(list);
		printSet(set);
	}

	public static void printDup(String[] s) {
		Set<String> set2 = new HashSet<String>();
		for (String i : s) {
			int cont = 0;
			for (String j : s) {
				if (i.equals(j)) {
					cont++;
				}
			}
			if (cont > 1) {
				set2.add(i);
			}
		}
		printSet(set2);
	}

	public static void printDuplicatesProfe(String[] array) {
		Set<String> set = new HashSet<String>();
		Set<String> duplicates = new HashSet<String>();
		for (String s : array) {
			if (!set.add(s)) {
				duplicates.add(s);
			}
		}
		for (String s : duplicates) {
			System.out.println(s);
		}
	}

	public static <E> Set<E> intersection(Set<E> s1, Set<E> s2) {

		Set<E> set = new HashSet<E>(s1);
		Set<E> set2 = new HashSet<E>();
		for (E s : s2) {
			if (!set.add(s)) {
				set2.add(s);
			}
		}
		return set2;

	}

}
